<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
  <!--<![endif]-->
  <!-- BEGIN HEAD -->

  <head>
    <meta charset="utf-8" />
    <title>Secretaria de Innovación | </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #3 for " name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/layouts/layout3/css/themes/yellow-crusta.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
  </head>
  <!-- END HEAD -->

  <body class="page-container-bg-solid page-header-menu-fixed">
    <div class="page-wrapper">
      <div class="page-wrapper-row">
        <div class="page-wrapper-top">
          <!-- BEGIN HEADER -->
          <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
              <div class="container">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                  <a href="index.html">
                    <img src="/assets/layouts/layout3/img/logo-default.jpg" alt="logo" class="logo-default">
                  </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
              </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
              <div class="container">

                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu ">
                  <ul class="nav navbar-nav">
                    <li aria-haspopup="true" class="">
                      <a href="javascript:;"> Inicio
                        <span class="arrow"></span>
                      </a>
                    </li>

                    <li aria-haspopup="true" class="">
                      <a href="{{ route('login') }}"> Inicio de Sesión
                        <span class="arrow"></span>
                      </a>
                    </li>

                    <li aria-haspopup="true" class="">
                      <a href="{{ route('password.request') }}"> Recuperar contraseña
                        <span class="arrow"></span>
                      </a>
                    </li>

                    <li aria-haspopup="true" class="">
                      <a href="{{ route('register') }}"> Registrarse
                        <span class="arrow"></span>
                      </a>
                    </li>

                  </ul>
                </div>
                <!-- END MEGA MENU -->
              </div>
            </div>
            <!-- END HEADER MENU -->
          </div>
          <!-- END HEADER -->
        </div>
      </div>
      <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
          <!-- BEGIN CONTAINER -->
          <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
              <!-- BEGIN CONTENT BODY -->
              <!-- BEGIN PAGE HEAD-->
              <div class="page-head">
                <div class="container">
                  <!-- BEGIN PAGE TITLE -->
                  <div class="page-title">
                    <h1>Blank Page</h1>
                  </div>
                  <!-- END PAGE TITLE -->
                </div>
              </div>
              <!-- END PAGE HEAD-->
              <!-- BEGIN PAGE CONTENT BODY -->
              <div class="page-content">
                <div class="container">
                  <!-- BEGIN PAGE BREADCRUMBS -->
                  @yield('breadcrumbs')
                  <!-- END PAGE BREADCRUMBS -->
                  <!-- BEGIN PAGE CONTENT INNER -->
                  <div class="page-content-inner">
                    @yield('content')

                  </div>
                  <!-- END PAGE CONTENT INNER -->
                </div>
              </div>
              <!-- END PAGE CONTENT BODY -->
              <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

          </div>
          <!-- END CONTAINER -->
        </div>
      </div>
      <div class="page-wrapper-row">
        <div class="page-wrapper-bottom">
          <!-- BEGIN FOOTER -->
          <!-- BEGIN PRE-FOOTER -->
          <div class="page-prefooter">
            <div class="container">
              <div class="row">

                <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                  <h2>Nosotros</h2>
                  <p>Aplicación de Rubricas de la Facultad de Medicina de la
                    Universidad Autónoma de Nuevo León. </p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                  <h2>Siguenos</h2>
                  <ul class="social-icons">
                    <li>
                      <a href="javascript:;" data-original-title="rss" class="rss"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="googleplus" class="googleplus"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="linkedin" class="linkedin"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="youtube" class="youtube"></a>
                    </li>
                    <li>
                      <a href="javascript:;" data-original-title="vimeo" class="vimeo"></a>
                    </li>
                  </ul>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12 footer-block">
                  <h2>Contacto</h2>
                  <address class="margin-bottom-40"> Phone: 818 347 4668
                    <br> Email:
                    <a href="mailto:info@metronic.com">antsanchez@gmail.com</a>
                  </address>
                </div>

              </div>
            </div>
          </div>
          <!-- END PRE-FOOTER -->
          <!-- BEGIN INNER FOOTER -->
          <div class="page-footer">
            <div class="container"> 2018 &copy; Tema Metronic por
              <a target="_blank" href="http://keenthemes.com">Keenthemes</a>
            </div>
          </div>
          <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
          </div>
          <!-- END INNER FOOTER -->
          <!-- END FOOTER -->
        </div>
      </div>
    </div>
    <!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script>
<script src="/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
    <!-- BEGIN CORE PLUGINS -->
    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
    <script src="/assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>
    <script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
    <script>
      $(document).ready(function()
      {
        $('#clickmewow').click(function()
        {
          $('#radio1003').attr('checked', 'checked');
        });
      })
    </script>
  </body>

</html>
