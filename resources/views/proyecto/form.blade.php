@extends("layouts.main")

@section('content')
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>{{ $proyecto}} </div>
    </div>
    <div class="portlet-body form">
      <!-- BEGIN FORM-->
      <form class="horizontal-form" role="form" action="{{ route('proyecto.update', $proyecto) }}" method="POST">
        {{ method_field('PUT') }}

        {{ csrf_field() }}
        <div class="form-body">
          <h4 class="form-section">Datos Generales</h4>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Titulo:</label>
                <input type="text" name="titulo" value="{{ old('titulo', $proyecto->titulo) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->

          <h4 class="form-section">Investigador Principal</h4>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label"> Investigador Principal:</label>
                <input type="text" name="inv_princ" value="{{ old('inv_princ', $proyecto->inv_princ) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Departamento/Servicio/Unidad/Área/Centro:</label>
                <input type="text" name="inv_princ_area" value="{{ old('inv_princ_area', $proyecto->inv_princ_area) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->

          <h4 class="form-section">Persona de Contacto</h4>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Nombre:</label>
                <input type="text" name="titulo" value="{{ old('titulo', $proyecto->titulo) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Correo Electronico:</label>
                <input type="text" name="email" value="{{ old('email', $proyecto->email) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Tel. Oficina:</label>
                <input type="text" name="tel_oficina" value="{{ old('tel_oficina', $proyecto->tel_oficina) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Tel. Movil:</label>
                <input type="text" name="telefono" value="{{ old('telefono', $proyecto->telefono) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
        </div>

        <div class="form-actions">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <a href="{{ route('dashboard') }}" class="btn default"><i class="fa fa-remove"></i> Cancelar</a>
                  <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Guardar Cambios</button>
                </div>
              </div>
            </div>
            <div class="col-md-6"> </div>
          </div>
        </div>
      </form>
      <!-- END FORM-->
    </div>
  </div>
@endsection
