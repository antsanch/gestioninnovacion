@extends("layouts.main")

@section('content')
<div class="row">
  <div class="col-md-10">
    <div class="portlet box yellow">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-cogs"></i>Mis Proyectos </div>
      </div>
      <div class="portlet-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th> # </th>
                <th> Nombre </th>
                <th nowrap> Investigador Principal </th>
                <th nowrap> Avance Reportado </th>
                <th nowrap> Acciones </th>
              </tr>
            </thead>
            <tbody>
@foreach($proyectos as $item)
              <tr>
                <td> {{ $loop->iteration }} </td>
                <td><a href="{{ route('proyecto.edit', $item) }}">{{ $item->titulo }} </a></td>
                <td> {{ $item->inv_princ }} </td>
                <td nowrap> {{ $item->avance }} </td>
                <td nowrap> Table cell </td>
              </tr>
@endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
