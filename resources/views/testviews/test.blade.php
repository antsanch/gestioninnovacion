@extends('layouts.main')

@section('content')
  <div class="row">
    <label class="col-md-12 control-label" for="person">Pruebas:</label>
    <input type="text" id="person" class="typeahead form-control" autocomplete="off">
  </div>
@endsection

@section('javascript')
  <script src="js/typeahead.bundle.min.js" type="text/javascript"></script>
  <script>
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };

    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
      'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
      'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
      'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
      'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
      'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
      'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
      'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
      'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];

    $('#person').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'states',
      source: substringMatcher(states),
      templates: {
        notFound: '<div>Not Found</div>',   /* Rendered if 0 suggestions are available */
        pending: '<div>Loading...</div>',   /* Rendered if 0 synchronous suggestions available
                                               but asynchronous suggestions are expected */
        header: '<div>Found Records:</div>',/* Rendered at the top of the dataset when
                                               suggestions are present */
        suggestion:  function(data) {       /* Used to render a single suggestion */
                        return '<div>'+ data +'</div>'
                     },
        footer: '<div>Footer Content</div>',/* Rendered at the bottom of the dataset
                                               when suggestions are present. */
      }
    });
  </script>
@endsection
