@extends("layouts.main")

@section('content')
<div class="row">

  <div class="portlet box green-jungle col-md-3">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gears"></i>Menú</div>
      </div>
      <div class="portlet-body">
        <ul>
          <li><a href="{{ route('perfil.show') }}">Ver Perfil</a></li>
          <li><a href="{{ route('perfil.edit') }}">Editar Perfil</a></li>
          <li><a href="{{ route('perfil.search') }}"title="Busca perfiles huerfanos y los integra a tu información">Integrar Perfiles</a></li>
        </ul>
      </div>
  </div>

  <div class="portlet box blue col-md-9">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Mi Perfil </div>
    </div>
    <div class="portlet-body form">
      <!-- BEGIN FORM-->
      <form class="form-horizontal" role="form">
        <div class="form-body">
          <h3 class="form-section">Datos Personales</h3>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold bold">Nombre:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->name }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold"> Apellido Paterno:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->app }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Apellido Materno:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->apm }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Correo Electronico:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->email }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Área:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->area }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Puesto:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->puesto }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Tel. Oficina:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->tel_oficina }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-4 bold">Tel. Movil:</label>
                <div class="col-md-8">
                  <p class="form-control-static"> {{ $profile->telefono }} </p>
                </div>
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
        </div>
        <div class="form-actions">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <a href="{{ route('dashboard') }}" class="btn default"><i class="fa fa-remove"></i> Regresar</a>
                  <a href="{{ route('perfil.edit') }}" class="btn green-jungle"><i class="fa fa-pencil"></i> Editar</a>
                </div>
              </div>
            </div>
            <div class="col-md-6"> </div>
          </div>
        </div>
      </form>
      <!-- END FORM-->
    </div>
  </div>
</div>
@endsection
