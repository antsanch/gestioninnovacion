@extends("layouts.main")

@section('content')
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i> Mi Perfil
      </div>
    </div>
    <div class="portlet-body form">
      <!-- BEGIN FORM-->
@if($profile->exists)
      <form class="horizontal-form" role="form" action="{{ route('perfil.update') }}" method="POST">
        {{ method_field('PUT') }}
@else
      <form class="horizontal-form" role="form" action="{{ route('perfil.store') }}" method="POST">
@endif
        {{ csrf_field() }}
        <div class="form-body">
          <h3 class="form-section">Datos Personales</h3>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label bold">Nombre:</label>
                <input type="text" name="name" value="{{ old('name', $profile->name) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold"> Apellido Paterno:</label>
                <input type="text" name="app" value="{{ old('app', $profile->app) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Apellido Materno:</label>
                <input type="text" name="apm" value="{{ old('apm', $profile->apm) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
<!--
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Correo Electronico:</label>
                <input type="text" name="email" value="{{ old('email', $profile->email) }}" class="form-control">
              </div>
            </div>
-->
            <!--/span-->
          </div>
          <!--/row-->
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Área:</label>
                <input type="text" name="area" value="{{ old('area', $profile->area) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Puesto:</label>
                <input type="text" name="puesto" value="{{ old('puesto', $profile->puesto) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Tel. Oficina:</label>
                <input type="text" name="tel_oficina" value="{{ old('tel_oficina', $profile->tel_oficina) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label bold">Tel. Movil:</label>
                <input type="text" name="telefono" value="{{ old('telefono', $profile->telefono) }}" class="form-control">
              </div>
            </div>
            <!--/span-->
          </div>
          <!--/row-->
        </div>
        <div class="form-actions">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <a href="{{ route('dashboard') }}" class="btn default"><i class="fa fa-remove"></i> Cancelar</a>
                  <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Guardar Cambios</button>
                </div>
              </div>
            </div>
            <div class="col-md-6"> </div>
          </div>
        </div>
      </form>
      <!-- END FORM-->
    </div>
  </div>
@endsection
