@extends("layouts.main")

@section('content')
<p>Se han encontrado proyectos registrados para este usuario</p>

@foreach($profiles as $profile)
  <table>
    <tr>
      <td>
        <p><strong>Nombre:</strong> {{ $profile }}</p>
      </td>
      <td>
        <p><strong>Correo electronico:</strong> {{ $profile->email}} </p>
      </td>
    </tr>
    <tr>
      <td colspan="2">

        <table class="table table-bordered">
          <tr>
            <th>Proyecto(s) donde participa</th>
            <th>Investigador Principal</th>
            <th>Avance Reportado</th>
          </tr>
@foreach($profile->proyectos as $proyecto)
          <tr>
            <td>{{ $proyecto->titulo }}</td>
            <td width="20%">{{ $proyecto->inv_princ }}</td>
            <td width="20%">{{ $proyecto->avance }}</td>
          </tr>
@endforeach
        </table>

      </td>
    </tr>
    <tr>
      <td class="text-right" colspan="2">
        <a href="asociar/{{ $profile->id }}" class="btn green-jungle"> Asociar con este Perfil</a>
      </td>
    </tr>
  </table>
@endforeach


@endsection
