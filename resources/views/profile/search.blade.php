@extends("layouts.main")

@section('stylesheet')
@endsection

@section('content')
<div class="row">
  <form>
    <label for="search" class="col-md-2 control-label bold">Palabas a buscar: </label>
    <div class="col-md-9">
      <input type="search" name="q" id="search" value="{{ $q }}" placeholder="Frase a buscar" class="col-md-9 form-control">
    </div>
    <button type="submit" class="btn green col-md-1"><i class="fa fa-search"></i> Buscar</button>
  </form>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="portlet box yellow">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-cogs"></i>Perfiles posiblemente duplicados
        </div>
      </div>
      <div class="portlet-body form">
        <form action="{{ route('perfil.unificar') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th> Id </th>
                    <th> Nombre </th>
                    <th nowrap> eMail </th>
                    <th nowrap> Télefono </th>
                    <th nowrap> Oficina </th>
                    <th nowrap> Área </th>
                    <th nowrap> Puesto </th>
                    <th> Proys</th>
                    <th> Menú</th>
                  </tr>
                </thead>
                <tbody>
@foreach($perfiles as $item)
@if($item->user_id == $user->id)
                  <tr class="warning">
@else
                  <tr>
@endif
                    <input name="items[]" type="hidden" value="{{ $item->id }}">
                    <td> {{ $item->id }} </td>
                    <td><a href="{{ route('proyecto.edit', $item) }}">{{ $item->fullName }} </a></td>
                    <td> {{ $item->email }} </td>
                    <td nowrap> {{ $item->telefono }} </td>
                    <td nowrap> {{ $item->tel_oficina }} </td>
                    <td> {{ $item->area }} </td>
                    <td> {{ $item->puesto }} </td>
                    <td nowrap> {{ count($item->proyectos) }} </td>
                    <td>
<!--
                      <button type="button" class="btn red delete" title="Remover de la lista"><i class="fa fa-close"></i></button>
-->
                      <a class="delete"><i class="fa fa-close"></i></a>
                    </td>
                  </tr>
@endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-12 text-right">
                <button type="submit" class="btn green"><i clas="fa fa-save"></i> Unificar Perfiles</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script>
  $(function() {
    $('table').on('click', '.delete', function() {
      $(this).parents('tr').remove();
      //~ alert('se borra');
      //~ console.log($(this).parents('tr'));
    });

  });
</script>
@endsection
