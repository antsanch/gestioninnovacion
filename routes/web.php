<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function() {
  return view('testviews.test');
});

Route::get('perfil', 'profileController@show')->name('perfil.show');
Route::get('perfil/crear', 'profileController@create')->name('perfil.create');
Route::post('perfil', 'profileController@store')->name('perfil.store');
Route::post('perfil/unificar', 'profileController@unificar')->name('perfil.unificar');
Route::get('perfil/editar', 'profileController@edit')->name('perfil.edit');
Route::put('perfil/', 'profileController@update')->name('perfil.update');
Route::get('perfil/json', 'profileController@json')->name('perfil.json');
Route::get('perfil/search', 'profileController@search')->name('perfil.search');
Route::get('perfil/asociar', 'profileController@mostrar')->name('profile.buscar');
Route::get('perfil/asociar/{id}', 'profileController@asociar');

Route::get('dashboard', 'mainController@dashboard')->name('dashboard');

Route::resource('proyecto', 'proyectoController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
