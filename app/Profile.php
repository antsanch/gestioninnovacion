<?php

namespace gestionInnovacion;

//~ use gestionInnovacion\TextUtils;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Profile extends Model
{
  use SearchableTrait;

  protected $table = "profile";

  protected $fillable = [
    'user_id',
    'name',
    'app',
    'apm',
    'cargo',
    'area',
    'puesto',
    'telefono',
    'tel_oficina'
  ];

  protected $hidden = [
    'name',
    'app',
    'apm',
    'cargo',
    'created_at',
    'updated_at'
  ];

  protected $appends = [
    'full_name'
  ];

  protected $searchable = [
    /**
     * Columns and their priority in search results.
     * Columns with higher values are more important.
     * Columns with equal values have equal importance.
     *
     * @var array
     */
    'columns' => [
      'name'  => 10,
      'app'   => 10,
      'apm'   => 10,
      'email' => 2,
      'area'  => 5,
    ]
  ];

  public function __toString() {
    return $this->fullname;
  }

  public function getFullNameAttribute() {
    return ucwords(strtolower(sprintf("%s %s %s", $this->name, $this->app, $this->apm )));
  }

  /**************************************************
   * RELATIONS
   * */
  public function user() {
    return $this->belongsTo('gestionInnovacion\User')
      ->withDefault();
  }

  public function vocero()
  {
    return $this->hasMany('gestionInnovacion\Proyecto', 'contacto_id');
  }

  public function proyectos()
  {
    return $this->belongsToMany('gestionInnovacion\Proyecto')->withTimestamps();
  }
}
