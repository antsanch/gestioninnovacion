<?php

namespace gestionInnovacion;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
  protected $table = 'proyectos';

  protected $fillable = [
    'titulo'
  ];

  public function __toString()
  {
    return $this->titulo;
  }

  /**
   * Get the protocolo record associated with the proyecto.
   */
  public function protocolo()
  {
    return $this->hasOne('GDriveConnector\Protocolo')->withDefault();
  }

  public function participantes()
  {
    return $this->belongsToMany('GDriveConnector\Profile')->withTimestamps();
  }

  /**
   * Get the contact record associated with the proyecto.
   */
  public function contacto()
  {
    return $this->belongsTo('GDriveConnector\Profile', 'contacto_id')->withDefault();
  }
}
