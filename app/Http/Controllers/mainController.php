<?php

namespace gestionInnovacion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class mainController extends Controller
{
  public function dashboard()
  {
    $user = Auth::user();
    $profile = $user->profile;
    $proyectos = $profile->proyectos;
    return view('main.dashboard', [
      'user'      =>  $user,
      'profile'   =>  $profile,
      'proyectos' =>  $proyectos
    ]);
  }
}
