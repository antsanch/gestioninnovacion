<?php

namespace gestionInnovacion\Http\Controllers;

use Illuminate\Http\Request;
use gestionInnovacion\Profile;
use Illuminate\Support\Facades\Auth;

class profileController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function mostrar()
  {
    return view('profile.asociar', [
      'profiles' => Profile::where([
        ['email', '=', Auth::user()->email],
        ['user_id', '=', null]
      ])->get()
    ]);
  }

  public function asociar(Request $request)
  {
    $user = Auth::user();
    $profile = Profile::where('id', $request->id)->first();
    $profile->user_id = $user->id;
    $profile->save();

    return redirect()->route('perfil.show');
  }

  public function json(Request $request)
  {
    $profiles = Profile::search($request->q)->get();
    return $profiles->toJson(JSON_UNESCAPED_UNICODE);
    //~ return Profile::all()->toJson(JSON_UNESCAPED_UNICODE);
  }

  public function search(Request $request)
  {
    $q=$request->q;
    $profiles = Profile::search($q)
      ->orderBy('id')
      ->where('user_id', null)
      ->with('proyectos')
      ->get();

    return view('profile.search', [
      'perfiles'  => $profiles,
      'user'      => Auth::user(),
      'q'         => $q
    ]);

    return $profiles;
  }

  public function unificar(Request $request)
  {
    $profile = Auth::user()->profile;

    foreach(Profile::find($request->items) as $dummy) {
      $profile->proyectos()->attach($dummy->proyectos);
      $dummy->proyectos()->detach($dummy->proyectos);
      $dummy->delete();
      //~ dump($dummy->proyectos);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $profile = new Profile();
    $profile->email = Auth::user()->email;

    return view('profile.form', [
      'profile' => $profile
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //~ dd($request->all());
    $profile = new Profile();
    $profile->fill($request->all());
    $profile->user_id = Auth::id();
    $profile->email = Auth::user()->email;
    $profile->save();

    return redirect()->route('perfil.show');
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function show()
  {
    $user = Auth::user();
    return view('profile.show', [
      'user'    =>  $user,
      'profile' =>  $user->profile
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function edit()
  {
    $user = Auth::user();

    return view('profile.form', [
      'profile' =>  $user->profile,
      'user'    =>  $user
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $profile = $user = Auth::user()->profile;
    $profile->fill($request->all());
    $profile->save();

    return redirect()->route('perfil.show');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}
